﻿// Uncomment the following to provide samples for PageResult<T>. Must also add the Microsoft.AspNet.WebApi.OData
// package to your project.
////#define Handle_PageResultOfT

#pragma warning restore CS1591
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Description;
using WebApiDemo.Models;

namespace WebApiDemo.Controllers
{
    /// <summary>
    /// 商品api
    /// </summary>
    [RoutePrefix("Products")]
    public class ProductsController : ApiController
    {
        private FabricsEntities db = new FabricsEntities();


        // prevent loop
        public ProductsController()
        {
            db.Configuration.LazyLoadingEnabled = false;
        }
        // GET: api/Products

        /// <summary>
        /// 取得所有商品
        /// </summary>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public IQueryable<Product> GetProduct()
        {
            return db.Product.OrderByDescending(a => a.ProductId).Take(10);
        }


        /// <summary>
        /// 取得特定商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Products/5
        [ResponseType(typeof(Product))]
        [Route("{id}", Name = "GetProductById")]
        [HttpGet]
        public IHttpActionResult GetProduct(int id)
        {
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        /// <summary>
        /// 測試 From Body
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [Route("TestFromBody")]
        [HttpPost]
        public IHttpActionResult TestFromBody([FromBody]string name)
        {
            return Ok("get : " + name);
        }



        /// <summary>
        /// 測試 From Uri
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        [Route("TestFromUri")]
        [HttpGet]
        public IHttpActionResult TestFromUri([FromUri]Point point)
        {
            return Ok(point);
        }

        public class Point
        {
            public double X { get; set; }
            public double Y { get; set; }

        }

        /// <summary>
        /// Test for client 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [ResponseType(typeof(Client))]
        [Route("Clinets/{id:int}")]
        public HttpResponseMessage GetClient(int id)
        {
            Client client = db.Client.Find(id);

            //Product product = db.Product.Find(id);
            if (client == null)
            {
                return new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.NotFound,
                    Content = new StringContent("找沒資料QQQ", Encoding.GetEncoding("utf-8"))
                };

            }

            //return Ok(product);
            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content =
                new ObjectContent<Client>(
                    client,
                    GlobalConfiguration.Configuration.Formatters.JsonFormatter)
            };
        }



        //限制為 int 
        /// <summary>
        /// 取得訂單 by 商品id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("{id:int}/OrderLines")]
        [HttpGet]
        public IHttpActionResult GetProductOrderLines(int id)
        {
            Product product = db.Product.Include("OrderLine")
                .FirstOrDefault(a => a.ProductId == id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product.OrderLine.ToList());
        }
        ///// <summary>
        ///// 使用訂單編號取得該訂單的產品
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[Route("Product/{id:int}/OrderProductName")]
        //[HttpGet]
        //public IHttpActionResult GetProductName(int id)
        //{
        //    Product product = db.OrderLine.FirstOrDefault(a => a.OrderId == id).Product

        //    return Ok(product);
        //}



        /// <summary>
        /// 更新商品
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        //[HttpPut]
        [Route("id")]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ProductId)
            {
                return BadRequest();
            }

            db.Entry(product).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// 新增商品
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        // POST: api/Products
        [ResponseType(typeof(Product))]
        [HttpPost]
        [Route("")]
        public IHttpActionResult CreateProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Product.Add(product);
            db.SaveChanges();

            return CreatedAtRoute("GetProductById", new { id = product.ProductId }, product);
        }
        /// <summary>
        /// 刪除商品
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: api/Products/5
        [ResponseType(typeof(Product))]
        [Route("id")]
        public IHttpActionResult DeleteProduct(int id)
        {
            Product product = db.Product.Find(id);
            if (product == null)
            {
                return NotFound();
            }

            db.Product.Remove(product);
            db.SaveChanges();

            return Ok(product);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProductExists(int id)
        {
            return db.Product.Count(e => e.ProductId == id) > 0;
        }
    }
}